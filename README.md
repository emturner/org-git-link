<!--
SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# About
`ol-gitlab.el` adds a `gitlab` link type to *Org mode*, supporting:
- issues
- merge requests
- milestones

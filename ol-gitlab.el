;;; ol-gitlab.el --- gitlab links for Org mode. -*- lexical-binding: t -*-
;;
;; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;;; Commentary:
;; Allow for easier links to gitlab issues, merge requests & milestones.
;;
;;; Code:
(require 'ol)
(require 'subr-x)

(defcustom org-gitlab-prefix "gitlab"
  "The org-link gitlab type prefix.
This must be set before ol-gitlab loads."
  :group 'org-link
  :type '(string))

(defcustom org-gitlab-base-url "https://gitlab.com"
  "The base url used to open org-gitlab links."
  :group 'org-link
  :type '(string))

(defcustom org-gitlab-default-project ""
  "Optionally set the default project to use for gitlab links."
  :group 'org-link
  :type '(string))

(org-link-set-parameters org-gitlab-prefix
                         :follow #'org-gitlab-open
                         :export #'org-gitlab-export
                         :store #'org-gitlab-store-link
                         :insert-description #'org-gitlab-insert-description)

(defun org-gitlab--type-char-p (type-char)
  "Return the long-string version of TYPE-CHAR if supported."
  (cond ((char-equal ?# type-char) "issues")
        ((char-equal ?! type-char) "merge_requests")
        ((char-equal ?% type-char) "milestones")
        ('t nil)))

(defun org-gitlab--get-type (link-ref)
  "Return the type of LINK-REF, and the reference number."
  (let ((type-char (org-gitlab--type-char-p (seq-first link-ref)))
        (ref-num (seq-rest link-ref)))
    (if type-char
        `(,type-char . ,ref-num)
      (error (format "ol-gitlab: Unknown type indicator: '%c'" type-char)))))

(defun org-gitlab--get-project (link-ref)
  "Return the project included in LINK-REF, or the default one configured.
You can override the default value by setting org-gitlab-default-project."
  (let* ((proj (seq-take-while (lambda (c) (not (org-gitlab--type-char-p c)))
                               link-ref))
         (ref (substring link-ref (seq-length proj)))
         (proj (if (string-empty-p proj)
                   org-gitlab-default-project proj)))
    `(,proj . ,ref)))

(defun org-gitlab--with-project (link-ref)
  "Add the default project on LINK-REF if it doesn't have one."
  (let ((with-proj (org-gitlab--get-project link-ref)))
    (concat (car with-proj) (cdr with-proj))))

(defun org-gitlab--make-link (link-ref)
  "Reconstruct a gitlab link from the LINK-REF."
  (let* ((base org-gitlab-base-url)
         (proj-with-rest (org-gitlab--get-project link-ref))
         (type-with-rest (org-gitlab--get-type (cdr proj-with-rest))))
    (string-join (list base
                       (car proj-with-rest)
                       "-"
                       (car type-with-rest)
                       (cdr type-with-rest))
                 "/")))

(defun org-gitlab-open (link-ref _)
  "Open gitlab LINK-REF in configured browser."
  (browse-url (org-gitlab--make-link link-ref)))

(defun org-gitlab-export (link-ref description format _)
  "Export a gitlab LINK-REF from Org files.
The DESCRIPTION is used where the requested FORMAT supports it."
  (let ((url (org-gitlab--make-link link-ref))
        (desc (or description (org-gitlab--with-project link-ref))))
    (pcase format
      (`html (format "<a href=\"%s\">%s</a>" url desc))
      (`latex (format "\\href{%s}{%s}" url desc))
      (`texinfo (format "@uref{%s,%s}" url desc))
      (`ascii (format "%s (%s)" url desc))
      (_ url))))

(defun org-gitlab-store-link ()
  "Store a link to a gitlab issue, merge request or milestone.
Currently unimplemented."
  nil)

(defun org-gitlab-insert-description (link-loc _)
  "Generate a description from LINK-LOC."
  (string-remove-prefix (concat org-gitlab-prefix ":") link-loc))

(provide 'ol-gitlab)
;;; ol-gitlab.el ends here
